const express = require('express')
const app = express()
const port = 3000

app.get('/',(req, res) => {
    res.send('SimpleApp Assignment!')
})

app.listen(port, () => {
    console.log('Application is listening at https://localhost:$(3000)')
})